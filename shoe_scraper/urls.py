from django.conf.urls import url, include

from rest_framework import routers

from shoe_api.viewsets import (
    BrandViewSet, CurrencyViewSet, ColorViewSet, CrawledSiteViewSet,
    ItemDetailsViewSet, ItemCategoryViewSet, OutsoleViewSet, LiningViewSet
)


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'item-details', ItemDetailsViewSet)
router.register(r'item-category', ItemCategoryViewSet)
router.register(r'brand', BrandViewSet)
router.register(r'currency', CurrencyViewSet)
router.register(r'crawled-site', CrawledSiteViewSet)
router.register(r'color', ColorViewSet)
router.register(r'outsole', OutsoleViewSet)
router.register(r'lining', LiningViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
]