import json

from bs4 import BeautifulSoup


def main():

    data_path = 'data/test_data.jl'

    with open(data_path) as file:
        for i in range(50):
            data = json.loads(file.readline())

            print(data.keys())
            # crawled_at format: %Y-%m-%d %H:%M:%S
            print(type(data['crawled_at']))
            # if data['page_type'] == 'product_listing':
            #     process_product_listing(data)
            # elif data['page_type'] == 'product_detail':
            #     process_product_detail(data)
            # else:
            #     print("Unknown page type: " + data['page_type'])



def process_product_listing(page):
    """Extracts relevant data from a product listing page.

    Args:
        page: String containing the page HTML

    Returns:
        List of dicts containing item details
    """

    soup = BeautifulSoup(page['body'], 'html.parser')

    product_list = soup.find('div', {'class': 'productList'})

    items = product_list.find_all('div', {'class': 'item'})

    output = []
    for idx, item in enumerate(items):

        item_data = {
            'item_id': item['rel'],
            'pos': idx,
            'ordered_by': page['ordering'],
            'categories': page['product_category'],
            'page_number': page['page_number'],
            'sort_index': create_sort_index(page['page_number'], idx),
        }

        output.append(item_data)

    return output

def process_product_detail(data):
    """Extracts relevant data from a product details page.

    Args:
        page: String containing the page HTML

    Returns:
        Dict containing item details
    """

    soup = BeautifulSoup(data['body'], 'html.parser')

    data_snippet = soup.find(id='richSnippetData')

    item_data = {
        'article_number': soup.find('span', {'itemprop': 'model'}).string.strip(),

        'brand': data_snippet.find('meta', {'itemprop': 'brand'})['content'],
        'currency': data_snippet.find('meta', {'itemprop': 'priceCurrency'})['content'],
        'price': data_snippet.find('meta', {'itemprop': 'price'})['content'],
        'categories': data_snippet.find('meta', {'itemprop': 'category'})['content'],

        'color': soup.find('dt', text='Kleur').find_next_sibling('dd').string,
        'outsole': soup.find('dt', text='Buitenzool').find_next_sibling('dd').string,
        'lining': soup.find('dt', text='Voering').find_next_sibling('dd').string
    }

    return item_data

def create_sort_index(page_num, idx):
    """Takes in a page number and a per-page index and returns
    a sorting index.

    Sorting index = 1e6 * page_num + idx

    Example:
    create_sort_index(page_num=0, idx=5) -> 5
    create_sort_index(page_num=1, idx=2) -> 1000002
    create_sort_index(page_num=4, idx=299) -> 4000299
    create_sort_index(page_num=5, idx=0) -> 5000000

    Args:
        page_num: int
        idx: int

    Returns:
        int
    """
    return int(page_num * 1e6 + idx)




if __name__ == '__main__':
    main()
