from rest_framework import serializers

from .fields import PaginatedRelationField
from .paginators import EntriesPaginator

from .models import (
    Brand, Currency, Color, CrawledSite, ItemCategory, ItemDetails, Outsole,
    Lining
)


class ItemCategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ItemCategory
        exclude = ['crawled_site']

class BrandSimpleSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Brand
        exclude = ['crawled_site']

class CurrencySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Currency
        exclude = ['crawled_site']

class CrawledSiteSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CrawledSite
        fields = '__all__'

    brands = PaginatedRelationField(
        BrandSimpleSerializer,
        paginator=EntriesPaginator,
        read_only=True,
        source='brand_set'
    )


class ColorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Color
        exclude = ['crawled_site']

class OutsoleSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Outsole
        exclude = ['crawled_site']

class LiningSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Lining
        exclude = ['crawled_site']

class ItemDetailsSerializer(serializers.HyperlinkedModelSerializer):
    categories = ItemCategorySerializer(many=True)
    brand = BrandSimpleSerializer()
    currency = CurrencySerializer()
    color = ColorSerializer()
    outsole = OutsoleSerializer()
    lining = LiningSerializer()

    class Meta:
        model = ItemDetails
        fields = '__all__'

class BrandListSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Brand
        fields = '__all__'


class BrandDetailsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Brand
        fields = '__all__'

    items_details = PaginatedRelationField(
        ItemDetailsSerializer,
        paginator=EntriesPaginator,
        read_only=True,
        source='itemdetails_set'
    )
