from django.db import models

from .base_model import BaseModel

class ItemDetails(BaseModel):
    price = models.FloatField(null=True, blank=True)
    article_number = models.CharField(max_length=64)

    brand = models.ForeignKey(
        'Brand',
        models.SET_NULL,
        blank=True,
        null=True
    )

    currency = models.ForeignKey(
        'Currency',
        models.SET_NULL,
        blank=True,
        null=True
    )

    categories = models.ManyToManyField('ItemCategory')

    color = models.ForeignKey(
        'Color',
        models.SET_NULL,
        blank=True,
        null=True
    )

    outsole = models.ForeignKey(
        'Outsole',
        models.SET_NULL,
        blank=True,
        null=True
    )

    lining = models.ForeignKey(
        'Lining',
        models.SET_NULL,
        blank=True,
        null=True
    )
