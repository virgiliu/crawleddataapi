from django.db import models

class CrawledSite(models.Model):
    url = models.URLField()
