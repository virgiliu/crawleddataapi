from django.db import models

from .base_model import BaseModel

class Color(BaseModel):
    label = models.CharField(max_length=64)
