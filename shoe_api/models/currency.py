from django.db import models

from .base_model import BaseModel

class Currency(BaseModel):
    label = models.CharField(max_length=64)
