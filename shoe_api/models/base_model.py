from django.db import models

class BaseModel(models.Model):
    class Meta:
        abstract = True

    crawled_site = models.ForeignKey(
        'CrawledSite',
        models.SET_NULL,
        blank=True,
        null=True
    )
