from django.db import models

from .base_model import BaseModel

class ItemOrdering(BaseModel):
    position = models.IntegerField()
    page_number = models.IntegerField()
    sort_index = models.IntegerField()

    captured_at = models.DateTimeField()

    ordered_by = models.ForeignKey(
        'OrderingCategory',
        models.SET_NULL,
        blank=True,
        null=True
    )

    details = models.ForeignKey(
        'ItemDetails',
        models.SET_NULL,
        blank=True,
        null=True
    )
