from django.db import models

from .base_model import BaseModel

class Outsole(BaseModel):
    label = models.CharField(max_length=64)
