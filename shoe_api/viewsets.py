from rest_framework import viewsets

from .serializers import (
    ItemDetailsSerializer, ItemCategorySerializer, BrandSimpleSerializer,
    BrandDetailsSerializer, BrandListSerializer, CurrencySerializer,
    CrawledSiteSerializer, ColorSerializer, OutsoleSerializer,
    LiningSerializer, ItemDetailsSerializer
)

from .models import (
    Brand, Color, CrawledSite, Currency, ItemDetails, ItemCategory, Outsole,
    Lining
)

class ItemDetailsViewSet(viewsets.ModelViewSet):
    queryset = ItemDetails.objects.all()
    serializer_class = ItemDetailsSerializer

class ItemCategoryViewSet(viewsets.ModelViewSet):
    queryset = ItemCategory.objects.all()
    serializer_class = ItemCategorySerializer

class BrandViewSet(viewsets.ModelViewSet):
    queryset = Brand.objects.all()

    def get_serializer_class(self):
        if self.action == 'list':
            return BrandListSerializer
        elif self.action == 'retrieve':
            return BrandDetailsSerializer

class CrawledSiteViewSet(viewsets.ModelViewSet):
    queryset = CrawledSite.objects.all()
    serializer_class = CrawledSiteSerializer

class CurrencyViewSet(viewsets.ModelViewSet):
    queryset = Currency.objects.all()
    serializer_class = CurrencySerializer

class ColorViewSet(viewsets.ModelViewSet):
    queryset = Color.objects.all()
    serializer_class = ColorSerializer

class OutsoleViewSet(viewsets.ModelViewSet):
    queryset = Outsole.objects.all()
    serializer_class = OutsoleSerializer

class LiningViewSet(viewsets.ModelViewSet):
    queryset = Lining.objects.all()
    serializer_class = LiningSerializer
