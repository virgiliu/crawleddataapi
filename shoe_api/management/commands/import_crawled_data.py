import os
import json

from datetime import datetime
from urllib.parse import urlparse

from django.core.management.base import BaseCommand, CommandError
from bs4 import BeautifulSoup

from shoe_api.models import (
    Brand, Color, CrawledSite, Currency, ItemCategory, ItemDetails,
    ItemOrdering, Lining, OrderingCategory, Outsole
)

# TODO: profile command and see why it's slow
# https://gist.github.com/tclancy/4236077
# https://stackoverflow.com/a/1693616/287491

class Command(BaseCommand):
    help = 'Converts and stores crawled data in DB.'

    def add_arguments(self, parser):
        parser.add_argument('file_path', type=str)

    def handle(self, *args, **options):
        file_path = options['file_path']

        print('Loading data from ' + file_path)

        start_time = datetime.now()

        if not os.path.isfile(file_path):
            raise CommandError('File does not exist: ' + file_path)

        self.store_data_from_crawl_dump(file_path)

        print("\n\nDone in {}".format(datetime.now() - start_time))


    def store_data_from_crawl_dump(self, file_path):
        """Parses and saves crawled data into the database.

        Assumes one crawl object per line.

        Args:
            file_path: Absolute or relative path to the dump file
        """

        with open(file_path, 'r', encoding='utf8') as data_file:
            line_count = 0

            # Count number of lines for progress tracking, shouldn't take more than a second
            # TODO: compare with running wc -l, maybe better performance?
            for line in data_file:
                line_count += 1


            # Reset the file read pointer
            data_file.seek(0)

            for line_num, line in enumerate(data_file):

                page = json.loads(line)

                try:
                    if page['page_type'] == 'product_listing':
                        self.store_product_listing(
                            self.process_product_listing(page),
                            page['crawled_at']
                        )

                    elif page['page_type'] == 'product_detail':
                        self.store_item_details(self.process_product_detail(page))
                    else:
                        raise CommandError("Unknown page type: " + line['page_type'])

                    # Rudimentary progress display
                    print(
                        '{} entries. {} done, {} remaining'.format(line_count, line_num, line_count - line_num),
                        end='\r',
                        flush=True
                    )

                except Exception as err:
                    with open('out_debug.html', 'w', encoding='utf8') as outfile:
                        outfile.write(page['body'])
                    raise err


    def process_product_listing(self, page):
        """Extracts relevant data from a product listing page.

        Args:
            page: Dict containing the page HTML in the `body`key

        Returns:
            List of dicts containing item details
        """

        soup = BeautifulSoup(page['body'], 'html.parser')

        product_list = soup.find('div', {'class': 'productList'})

        items = product_list.find_all('div', {'class': 'item'})

        output = []
        for idx, item in enumerate(items):

            item_data = {
                'item_id': item['rel'],
                'position': idx,
                'ordered_by': page['ordering'],
                'categories': page['product_category'],
                'page_number': page['page_number'],
                'sort_index': self.create_sort_index(page['page_number'], idx),
                'url': self.get_domain_from_url(page['page_url'])
            }

            output.append(item_data)

        return output


    def process_product_detail(self, data):
        """Extracts relevant data from a product details page.

        Args:
            data: Dict containing the page HTML in the `body` key

        Returns:
            Dict containing item details
        """

        soup = BeautifulSoup(data['body'], 'html.parser')

        data_snippet = soup.find(id='richSnippetData')

        item_data = {
            'article_number': soup.find('input', {'id': 'hdnProductId'}).get('value').strip(),

            'brand': data_snippet.find('meta', {'itemprop': 'brand'})['content'],
            'currency': data_snippet.find('meta', {'itemprop': 'priceCurrency'})['content'],
            'price': data_snippet.find('meta', {'itemprop': 'price'})['content'],

            'color': self.get_value_from_desc_list(soup, 'Kleur'),
            'outsole': self.get_value_from_desc_list(soup, 'Buitenzool'),
            'lining': self.get_value_from_desc_list(soup, 'Voering'),
            'url': self.get_domain_from_url(data['page_url'])
        }

        return item_data

    def create_sort_index(self, page_num, idx):
        """Takes in a page number and a per-page index and returns
        a sorting index.

        Sorting index = 1e6 * page_num + idx

        Example:
        create_sort_index(page_num=0, idx=5) -> 5
        create_sort_index(page_num=1, idx=2) -> 1000002
        create_sort_index(page_num=4, idx=299) -> 4000299
        create_sort_index(page_num=5, idx=0) -> 5000000

        Args:
            page_num: int
            idx: int

        Returns:
            int
        """
        return int(page_num * 1e6 + idx)

    def store_item_details(self, product_info):
        """Stores item details and creates any required entries like color, brand, etc.

        Args:
            product_info: Dict with details. See `process_product_details()`

        """

        site, site_created = CrawledSite.objects.get_or_create(url=product_info['url'])

        # TODO: could use mapping instead of hand writing each field
        # TODO: cache colors, brands, etc so we don't hit the DB so often
        #pylint: disable=W0612
        color, created = Color.objects.get_or_create(label=product_info['color'], crawled_site=site)
        brand, created = Brand.objects.get_or_create(label=product_info['brand'], crawled_site=site)
        currency, created = Currency.objects.get_or_create(label=product_info['currency'], crawled_site=site)
        outsole, created = Outsole.objects.get_or_create(label=product_info['outsole'], crawled_site=site)
        lining, created = Lining.objects.get_or_create(label=product_info['lining'], crawled_site=site)
        #pylint: enable=W0612

        item_details, created = ItemDetails.objects.get_or_create(
            article_number=product_info['article_number'],
            crawled_site=site
        )

        item_details.price=product_info['price']
        item_details.color=color
        item_details.brand=brand
        item_details.currency=currency
        item_details.outsole=outsole
        item_details.lining=lining

        item_details.save()



    def get_value_from_desc_list(self, soup, header_text):
        """Looks for a description list header element (`dt`) with the
        `header_text` value inside it, then gets the value from the sibling
        `dd` element.

        Useful when values in pages aren't tagged but can be looked up by their
        description list headers.

        Args:
            soup: BeautifulSoup object of the page
            header_text: The header text to look for

        Returns:
            String: The value under the header if it exists, else "n/a"
        """

        header = soup.find('dt', text=header_text)

        if header:
            return header.find_next_sibling('dd').string

        return 'n/a'


    def store_product_listing(self, data, captured_at):
        """Stores information about products which was extracted from
        listing view.

        Args:
            data: List of dicts containing information about products
            captured_at: Datetime string
        """

        for product_sort_info in data:
            site, site_created = CrawledSite.objects.get_or_create(url=product_sort_info['url'])
            details, det_created = ItemDetails.objects.get_or_create(article_number=product_sort_info['item_id'], crawled_site=site)

            # Add categories to the crawled product
            # TODO: cache categories
            for category in product_sort_info['categories']:
                # Create category if it doesn't exist
                category, cat_created = ItemCategory.objects.get_or_create(label=category, crawled_site=site)

                details.categories.add(category)

            # TODO: cache categories
            ordered_by, created = OrderingCategory.objects.get_or_create(label=product_sort_info['ordered_by'], crawled_site=site)

            ordering_entry = ItemOrdering(
                position=product_sort_info['position'],
                ordered_by=ordered_by,
                page_number=product_sort_info['page_number'],
                sort_index=product_sort_info['sort_index'],
                captured_at=captured_at,
                details=details,
                crawled_site=site
            )

            ordering_entry.save()

    def get_domain_from_url(self, url):
        """Gets the domain from a URL.

        Example:
        get_domain_from_url('https://www.google.com/search?q=blah') -> 'www.google.com'

        Args:
            url: URL from which to get the domain

        Returns:
            String. The domain extracted from the URL
        """
        parsed_uri = urlparse(url)
        result = '{uri.netloc}'.format(uri=parsed_uri)
        return result
